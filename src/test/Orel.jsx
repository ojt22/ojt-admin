import Hospital from "../pages/Hospital"
import Users from "../pages/Users"
import Speaker from "../pages/Speaker"
import { useContext, useEffect, useState } from "react";
import DataContext from "../context/dataContext";
import Loader from "../components/Loader";

export default function Orel() {

    const { createAlertMessage, setShowLoader } = useContext(DataContext);
    let [text, setText] = useState("enter new text")

    function showLoaderToSecond(time) {
        setShowLoader(true)
        setTimeout(() => { setShowLoader(false) }, 2000)
    }

    return (
        <div>
            <div>
                <br />
                <h4>1) - Test of 3 type of alert message</h4>
                <input value={text} onChange={(e)=>{setText(e.target.value)}}/>
                <br />
                <button onClick={() => { createAlertMessage(text, 1) }}>success</button>
                <button onClick={() => { createAlertMessage(text, 2) }}>warning</button>
                <button onClick={() => { createAlertMessage(text, 3) }}>error</button>
                <br />
                <br />
                <h4>2) - Test of Start Loader along 2 second</h4>
                <button onClick={() => { showLoaderToSecond(2000) }}>show loader</button>
                <br />
                <br />
            </div>
            <h1>Orel</h1>
            <Users />
            <Hospital />
            {/* <Speaker /> */}
        </div>
    )
}