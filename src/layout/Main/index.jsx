import MainRouter from "../../routes/MainRouter";
import AlertMessage from "../../components/AlertMessage";
import Loader from "../../components/Loader";

export default function Main() {
    return (
        <>
            <MainRouter />
            <AlertMessage/>
            <Loader />
        </>
    )
}