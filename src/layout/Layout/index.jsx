import Main from '../Main'
import Header from '../Header'
import Nav from '../Nav'
import Popup from '../Popup'
import { useContext, useEffect } from 'react'
import apiRequest from '../../fanctions/apiRequest'
import { useNavigate } from 'react-router-dom'
import dataContext from "../../context/dataContext"


export default function Layout() {
    let { setUsers } = useContext(dataContext)
    let nav = useNavigate()
    useEffect(() => {

        // Take token fron localStorage
        let token = localStorage.token
        console.log("token = ", token);

        // Check token and if the token expired the user redirected to login
        if (!token || !checkToken(token)) {
            // TODO = delete return
            return;

            // Delete token from localStorage
            delete localStorage.token

            // Initializing the data of user in context
            setUsers(null)

            // Redirecting the user to the login page
            nav("/login")
        }
    }, [])

    function checkToken(token) {
        // TODO  =   API request to server to check token
        let response = apiRequest(token)
        return response;
    }

    return (
        <div>
            <Header />
            <Main />
            <Nav />
            <Popup />
        </div>
    )
}