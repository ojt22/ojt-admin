import styles from "./style.module.css";

import Icon from "../../components/Icon";

import { AiOutlineHome, AiOutlinePoweroff } from "react-icons/ai";
import {
  BsRecordCircle,
  BsFillCollectionPlayFill,
  BsBullseye,
} from "react-icons/bs";
import { useEffect, useState } from "react";

export default function Header() {
  let [emphasis, setEmphasis] = useState({
    home: true,
    record: false,
    playlist: false,
    live: false,
    dani: false,
  });

  let { home, record, playlist, live} = emphasis;

  let changeEmphasis = (text) => {
    let replaceText = text.replace(" ", "");

    let copyEmphasis = { ...emphasis };

    for (let key in copyEmphasis) {
      copyEmphasis[key] = key == replaceText;
    }
    setEmphasis(copyEmphasis);
  };
  let arr = [
    {
      text: "home",
      icon: <AiOutlineHome />,
      emphasis: home,
    },
    {
      text: "record",
      icon: <BsRecordCircle />,
      emphasis: record,
    },
    {
      text: "play list",
      icon: <BsFillCollectionPlayFill />,
      emphasis: playlist,
    },
    {
      text: "live",
      icon: <BsBullseye />,
      emphasis: live,
    },
  ];
  return (
    <div className={styles.header}>
      <div className={styles.wrap1}>
        {arr.map((v,i) => {
          return (
            <Icon
              text={v["text"]}
              icon={v["icon"]}
              emphasis={v["emphasis"]}
              onClick={changeEmphasis}
              key={i}
            />
          );
        })}
      </div>

      <div className={styles.wrap2}>
        <Icon text={"john doe"} icon={<AiOutlinePoweroff />} />
      </div>
    </div>
  );
}
