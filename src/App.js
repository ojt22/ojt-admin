import './App.css';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Layout from './layout/Layout';
import MainTest from './test/MainTest';
import ContextManagment from './context/contextManagment';
import SignIn from './pages/SignIn';
import SignUp from './pages/SignUp';

export default function App() {
	return (
		<div className="App">
			<ContextManagment>
				<BrowserRouter>
					<Routes>
						<Route path="/login" element={<SignIn />}></Route>
						<Route path="/register" element={<SignUp />}></Route>
						<Route path="*" element={<><MainTest /><Layout /></>}></Route>
					</Routes>
				</BrowserRouter>
			</ContextManagment>
		</div>
	);
}