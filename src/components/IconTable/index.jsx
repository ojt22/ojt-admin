import { useContext } from "react";

import DataContext from "../../context/dataContext";

export default function IconTable({ Icon, PopUp, data , path }) {
  const { setIsOpen, setpopupComponent } = useContext(DataContext);

  function handleButtonClick() {
    setIsOpen(true);
    setpopupComponent(<PopUp data={data} path = {path}/>);
  }

  return (
    <div>
      <button onClick={PopUp && handleButtonClick}>{Icon && <Icon />}</button>
    </div>
  );
}
