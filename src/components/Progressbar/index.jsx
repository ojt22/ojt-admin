import styles from  "./style.module.css";

function ProgressBar(props) {
  let {par} = props;
  return (
    <div className={styles.generalDiv}>
      <div className={styles.progressbar}>
        <div className={styles.percent} style={{width: `${par > 100 ? 100 : par < 0 ? 0 : par}%`}}></div>
      </div>
      <div className={styles.progressPercent}>{par > 100 ? 100 : par < 0 ? 0 : par}%</div>
    </div>
  );  
}

export default ProgressBar;
