import styles from "./style.module.css";

export default function Icon({ icon, text, onClick, emphasis }) {
  
  let style = (emphasis == null) ? styles.headerButtonSelected : 
    (emphasis == true) ? styles.headerButtonSelected : 
    styles.headerButtonNoSelected;

  return (
    <div className={style} onClick={()=>{onClick(text)}}>
      <span className={styles.styleSpenIcon}>{icon}</span>
      <span className={styles.styleSpenText}>{text} </span>
    </div>
  );
}

  