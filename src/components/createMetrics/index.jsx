// import React, { useState } from "react";
import { useState } from "react";
import { useForm } from "react-hook-form";
import Comp from "../createMetricsComp";
// import Input from "../Input";
import styles from "./style.module.css";
import makeApiCall from "../../fanctions/apiRequest"

function NotePopup() {
  let [flagCancel, setFlagCancel] = useState(true);
  let [flagSave, setFlagSave] = useState(true);
  let [inputDate, setInputDate] = useState("");
  let [inputWeight, setInputWeight] = useState("");
  let [inputSize, setInputSize] = useState("");
  let [inputo2_Stauration, setInputo2_Stauration] = useState("");
  let [inputHeartRrate, setInputHeartRrate] = useState("");
  let [inputTemperature, setInputTemperature] = useState("");
  const arrSetInput = [
    setInputDate,
    setInputWeight,
    setInputSize,
    setInputo2_Stauration,
    setInputHeartRrate,
    setInputTemperature,
  ];

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();
  let dateRef = register("date", {
    required: true,
    minLength: 3,
  });
  function changeColor(e) {
    let button_type = e.target.innerHTML;
    if (button_type == "CANCLE" && flagCancel) {
      setColor1(color2);
      setColor2(color1);
      setFlagSave(true);
      setFlagCancel(false);
    } else if (button_type == "SAVE" && flagSave) {
      setColor1(color2);
      setColor2(color1);
      setFlagSave(false);
      setFlagCancel(true);
    }
  }

  let array = [
    { text: "Date", type: "date", required: false},
    { text: "Weight", type: "number", required: false },
    { text: "Size", type: "number", required: false },
    { text: "o2 Stauration", type: "number", required: false },
    { text: "Heart rate", type: "number", required: false },
    { text: "Temperature", type: "number", required: false },
  ];
  console.log(array[0].type);
  const [color1, setColor1] = useState({
    background: "#ffffff 0% 0% no-repeat padding-box",
    color: "blue",
  });
  const [color2, setColor2] = useState({
    background: "#0052a5 0% 0% no-repeat padding-box",
    color: "white",
  });

  function save() {
    let obj = {
      date: inputDate,
      weight: inputWeight,
      size: inputSize,
      o2_saturation: inputo2_Stauration,
      heart_rate: inputHeartRrate,
      temperature: inputTemperature,
    };
    makeApiCall("/baby_metrics", "post", obj)

  }

  return (
    <div className={styles.border}>
      <h1 className={styles.title}>CREATE / EDIT METRICS</h1>

      <div className={styles.containerComp}>
        {array.map((item, index) => {
          return (
            <Comp
              key={index}
              type={item.type}
              text={item.text}
              required={item.required}
              setInput={arrSetInput[index]}
            />
          );
        })}
      </div>

      <div className={styles.spaceButtons}>
        <button
          onClick={(e) => {
            changeColor(e);
          }}
          className={styles.button1}
          style={color2}
        >
          CANCLE
        </button>
        <button
          className={styles.button2}
          style={color1}
          onClick={(e) => {
            changeColor(e);
            save()
          }}
        >
          SAVE
        </button>
      </div>

      {/* <Input /> */}
    </div>
  );
}

export default NotePopup;
