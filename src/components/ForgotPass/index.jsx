import React, { useRef } from 'react';
import { useState } from "react";
import Input from '../Input';
import Button2 from '../Button';
import apiRequest from "../../fanctions/apiRequest"



const ForgotPass = () => {
    const [value,setValue] = useState()
    
    const removePopup = () => {
        console.log("Forgot Password");
    }
    const sendData = () => {
        console.log(value);
        apiRequest("http://localhost:8080/forgot_password","POST",value);
    }
    return (
        <div>
            <p>To create new password required your email</p>
            <Input label="Enter your email:" type="email" placeholder="email" value={value} setValue={setValue}/>
            <Button2 typeUnActive="secondary" typeInActive="primaryInActive" buttonText="Cancel" func={removePopup}/>         
            <Button2 typeUnActive="secondary" typeInActive="primaryInActive" buttonText="Send" func={sendData}/> 
        </div>
    );
}

export default ForgotPass;
