import { useContext, useEffect, useState } from "react"
import DataContext from "../../context/dataContext";
import styles from "./style.module.css"

export default function AlertMessage() {

	const { setAlertMessage, alertMessage } = useContext(DataContext);

	let [typeOfMessage, setTypeOfMessage] = useState({ 
		success: "", 
		warning: "", 
		error: "",
		// TO Complete
		// timeToStart: 1000,
		// timeToShow: 1000,
		// timeToEnd: 1000
	})

	let [timeToStart, setTimeToStart]= useState(1000);
	let [timeToShow, setTimeToShow]= useState(2000);
	let [timeToEnd, setTimeToEnd]= useState(1000);


	useEffect(() => {
		if (alertMessage) {
			setTypeOfMessage((e) => {
				return { ...e, ...alertMessage }
			})
		}
	}, [alertMessage])


	useEffect(() => {
		if (typeOfMessage.success) {
			createAlert(typeOfMessage.success, getStyle(1), deleteValueOfKey("success"))
		}
	}, [typeOfMessage.success])


	useEffect(() => {
		if (typeOfMessage.warning) {
			createAlert(typeOfMessage.warning, getStyle(2), deleteValueOfKey("warning"))
		}
	}, [typeOfMessage.warning])


	useEffect(() => {
		if (typeOfMessage.error) {
			createAlert(typeOfMessage.error, getStyle(3), deleteValueOfKey("error"))
		}
	}, [typeOfMessage.error])


	function deleteValueOfKey(key) {
		setAlertMessage((e) => { return { ...e, [key]: "" } })
	}


	// create aletr
	function createAlert(text, typeStyle, func) {
		let src = document.querySelector("#messages")

		let div1 = document.createElement("div")
		div1.className = typeStyle
		div1.style.transition = `opacity ${(timeToStart / 1000)}s`

		let div2 = document.createElement("div")
		div2.className = styles.containerMessage

		let span = document.createElement("span")
		span.className = styles.text
		span.innerHTML = text

		src.appendChild(div1)
		div1.appendChild(div2)
		div2.appendChild(span)

		setTimeout(() => {
			div1.style.opacity = 1
			setTimeout(() => {
				div1.style.transition = `opacity ${(timeToEnd / 1000)}s`
				div1.style.opacity = 0
				setTimeout(() => {
					div1.remove()
					func && func()
				}, timeToShow + timeToStart)
			}, timeToShow + timeToStart)
		}, 20)
	}

	function getStyle(type) {
		return type == 1 ? styles.alertSuccess : type == 2 ? styles.alertWarning : type == 3 ? styles.alertError : ""
	}

	return (
		<div id="messages" className={styles.containerAlertMessage}></div>
	)
}