import { useContext, useEffect, useRef, useState } from "react"
import DataContext from "../../context/dataContext";
import styles from "./style.module.css"

export default function AlertMessage() {

	const { setAlertMessage, alertMessage } = useContext(DataContext);

	let refStyle1 = useRef()
	let refStyle2 = useRef()
	let refStyle3 = useRef()

	let [typeOfMessage, setTypeOfMessage] = useState({ 
		success: "", 
		warning: "", 
		error: "",
		// TO Complete
		// timeToStart: 1000,
		// timeToShow: 1000,
		// timeToEnd: 1000
	})

	let [timeToStart, setTimeToStart]= useState(1000);
	let [timeToShow, setTimeToShow]= useState(2000);
	let [timeToEnd, setTimeToEnd]= useState(1000);


	useEffect(() => {
		if (alertMessage) {
			setTypeOfMessage((e) => {
				return { ...e, ...alertMessage }
			})
		}
	}, [alertMessage])


	useEffect(() => {
		if (typeOfMessage.success) {
			setTimeout(() => {
				refStyle1.current.style.opacity = 1;
				setTimeout(() => {
					refStyle1.current.style.opacity = 0;
					deleteData("success")
				}, timeToShow)
			}, 20)
		}
	}, [typeOfMessage.success])


	useEffect(() => {
		if (typeOfMessage.warning) {
			// console.log("typeOfMessage.warning = ",typeOfMessage.warning);
			setTimeout(() => {
				refStyle2.current.style.opacity = 1;
				setTimeout(() => {
					refStyle2.current.style.opacity = 0;
					deleteData("warning")
				}, timeToShow)
			}, 20)
		}
	}, [typeOfMessage.warning])


	useEffect(() => {
		if (typeOfMessage.error) {
			// console.log("typeOfMessage.error = ",typeOfMessage.error);
			setTimeout(() => {
				refStyle3.current.style.opacity = 1;
				setTimeout(() => {
					refStyle3.current.style.opacity = 0;
					deleteData("error")
				}, timeToShow)
			}, 20)
		}
	}, [typeOfMessage.error])


	function deleteData(key) {
		setTimeout(() => {
			setAlertMessage((e) => { return { ...e, [key]: "" } })
		}, timeToShow)
	}


	return (
		<div className={styles.containerAlertMessage}>
			{typeOfMessage.success &&
				<div ref={refStyle1} className={styles.alertSuccess}>
					<div className={styles.containerMessage}>
						<span className={styles.text}>{typeOfMessage.success}</span>
					</div>
				</div>
			}
			{typeOfMessage.warning &&
				<div ref={refStyle2} className={styles.alertWarning}>
					<div className={styles.containerMessage}>
						<span className={styles.text}>{typeOfMessage.warning}</span>
					</div>
				</div>
			}
			{typeOfMessage.error &&
				<div ref={refStyle3} className={styles.alertError}>
					<div className={styles.containerMessage}>
						<span className={styles.text}>{typeOfMessage.error}</span>
					</div>
				</div>
			}
		</div>

	)
}