import { useEffect, useState ,useContext} from "react";
import PropTypes from "prop-types";
import styles from "./style.module.css";
import DataContext from "../../context/dataContext";
const fixWordsOfHeader = (header = "") => {
  let ret = header;
  ret = ret.replace("id", "ID");
  ret = ret.replace("_", " ");
  ret = ret.replace("num", "number");
  ret = ret.replace("is active", "active");

  return ret;
};

// commponnet that response to create tabele

const ViewTable = ({ data={}, actions = [], theme = {}, filter = []}) => {
  const {setDataBaby} = useContext(DataContext);
  const [valueOfTable, setValueOfTable] = useState([]);
  const [headers, setHeaders] = useState();

  useEffect(() => {
    if (data.length > 0) {
      if (Object.keys(data[0]).length > 0) {
        setValueOfTable(data);
        filter.length > 0
          ? setHeaders(filter)
          : setHeaders(Object.keys(data[0]));
      } else {
        setValueOfTable();
      }
    } else {
      setValueOfTable();
    }
  }, [data]);
  return (
    // print all keys in the header of table
    // second map run at array, and print for each key your value
    <>
      {headers && (
        <table className={styles.table} style={{ ...theme.table }}>
          <thead style={theme.thead}>
            <tr style={theme.tr}>
              {headers.map((header, key) => {
                return (
                  <th className={styles.th} style={theme.th} key={key}>
                    {fixWordsOfHeader(header)}
                  </th>
                );
              })}
              {actions &&
                actions.map((action, key) => {
                  return (
                    <th className={styles.th} style={theme.th} key={key}>
                      {action.lable}
                    </th>
                  );
                })}
            </tr>
          </thead>
          <tbody>
            {valueOfTable &&
              valueOfTable.map((row, key) => {
                let keyToStyle = key;
                return (
                  <tr key={key} style={theme.tr}  >
                    {headers.map((value, key) => {
                      return (
                        <td                                                                
                          onDoubleClick={
                            ()=>{
                              setDataBaby(row);    
                            }
                          }
                          key={key}
                          className={
                            keyToStyle % 2 === 0 ? styles.td_1 : styles.td_2
                          }
                          style={theme.td}
                        >
                          {row[value] === null ? "" : String(row[value])}
                        </td>
                      );
                    })}
                    {actions &&
                      actions.map((action, key) => {
                        let Component = action.Component(row);

                        return (
                          <td
                            className={
                              keyToStyle % 2 === 0 ? styles.td_1 : styles.td_2
                            }
                            style={theme.td}
                            key={key}
                          >
                            {Component}
                          </td>
                        );
                      })}
                  </tr>
                );
              })}
          </tbody>
        </table>
      )}
    </>
  );
};

export default ViewTable;

ViewTable.propTypes = {
  data: PropTypes.array,
  actions: PropTypes.array,
  filter: PropTypes.array,
  theme: PropTypes.shape({
    table: PropTypes.object,
    thead: PropTypes.object,
    th: PropTypes.object,
    td: PropTypes.object,
    tr: PropTypes.object,
  }),
};
