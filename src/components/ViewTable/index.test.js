import { render, cleanup } from "@testing-library/react";
import renderer from "react-test-renderer";
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { shallow } from 'enzyme';
import ViewTable, { findLargestIndex } from "./index";


configure({ adapter: new Adapter() });



const sampleData = [
  { name: 'John Doe', age: 32, city: 'New York' },
  { name: 'Jane Doe', age: 29, city: 'San Francisco' },
];

const theme = {
  th: {
    backgroundColor: 'yellow'
  },
  td: {
    backgroundColor: 'red',
    color: 'white'
  },
}
describe('ViewTable component', () => {

  afterEach(() => cleanup())
  it('should match the snapshot', () => {
    const tree = renderer
      .create(<ViewTable data={sampleData} />).toJSON();
    expect(tree).toMatchSnapshot();
  })

  it('should render the table headers correctly', () => {
    const { getByText } = render(<ViewTable data={sampleData} />);
    expect(getByText('name')).toBeInTheDocument();
    expect(getByText('age')).toBeInTheDocument();
    expect(getByText('city')).toBeInTheDocument();

  });

  it('should render the correct data for each row', () => {
    const { getByText } = render(<ViewTable data={sampleData} />);
    expect(getByText('John Doe')).toBeInTheDocument();
    expect(getByText('Jane Doe')).toBeInTheDocument();
    expect(getByText('32')).toBeInTheDocument();
    expect(getByText('29')).toBeInTheDocument();
    expect(getByText('New York')).toBeInTheDocument();
    expect(getByText('San Francisco')).toBeInTheDocument();

  });
});


describe('ViewTable component', () => {
  it('should have the correct inline styles', () => {
    const wrapper = shallow(<ViewTable data={sampleData} theme={theme} />);
    const tableElement1 = wrapper.find('th').at(0);
    const tableElement2 = wrapper.find('td').at(0);
    expect(tableElement1.prop('style')).toHaveProperty('backgroundColor', 'yellow');
    expect(tableElement2.prop('style')).toHaveProperty('backgroundColor', 'red');
  });
});

