import styles from "./style.module.css";

function Note() {
  return (
    <div className={styles.border}>
      <div className={styles.header}>ADD NOTE</div>
      <div className={styles.inSideBorder}>
        <div className={styles.margin}><span>Note <span style={{color:"red"}}>*</span></span><br/>
      <input className={styles.styleInput} type="text"></input>
      <button className={styles.styleButton}>ADD NOTE</button>
      </div></div>
    </div>
  );
}

export default Note;
