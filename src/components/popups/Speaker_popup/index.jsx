import React, { useState } from "react";
import styles from './style.module.css'
import apiRequest from "../../../fanctions/apiRequest";
import { useContext } from "react";
import DataContext from "../../../context/dataContext";

function Speaker_popup(props) {
    const { setIsOpen, setpopupComponent } = useContext(DataContext);
    const [form, setForm] = useState(
        {
            start_working_date: props.data?.start_working_date ?? "",
        }
    );
    const [start_working_dateError, setStart_working_dateError] = useState("");
  
    const handleChange = (event) => {
        console.log("2023-01-21 00:00:00+00");
        const { name, value } = event.target;
        // console.log("value = ",value);
        let d = new Date()
        console.log(d);
        console.log("date = ",d = d.getDate);
        console.log("date = ",d = d.getDay);
        // console.log("date = ",let a = d.getFullYear);
        // console.log("date = ",d = d.getHours);
        // console.log("date = ",d = d.getMilliseconds);
        // console.log("date = ",d = d.getMinutes);
        // console.log("date = ",d = d.getMonth);
        // console.log("date = ",d = d.getSeconds);
        // console.log("date = ",d = d.getTime);
        // console.log("date = ",d = d.getTimezoneOffset);
        // console.log("date = ",d = d.getUTCDate);
        // console.log("date = ",d = d.getUTCDay);
        // console.log("date = ",d = d.getUTCFullYear);
        // console.log("date = ",d = d.getUTCHours);
        // console.log("date = ",d = d.getUTCMilliseconds);
        // console.log("date = ",d = d.getUTCMinutes);
        // console.log("date = ",d = d.getUTCMonth);
        // console.log("date = ",d = d.getUTCSeconds);
        // console.log("date = ",d = d.getVarDate);
        // console.log("date = ",d = d.toISOString);
        // console.log("date = ",d = d.toDateString);
        // console.log("date = ",d = d.toJSON);
        setForm({ ...form, [name]: value });
    };

    const handleSubmit = (event) => {
        let formIsValid = true;
        event.preventDefault();
    
        if (!form.start_working_date) {
            setStart_working_dateError("Please enter your start_working_date.");
            formIsValid = false;
        } else {
            setStart_working_dateError("");
        }
        if(formIsValid){
            apiRequest("speaker/", "POST", form ).then((response) => {
                console.log(form);
                    console.log(response);
                  });
        }
    };

    const handleCansel = () => {
        setIsOpen(false);
        setpopupComponent(null);
    };


    return (
        <div className={styles.form}>
            <h2>ADD/CREATE NEW SPEAKRE</h2>
            <form>
                <label className={styles.label}>start_working_date<span className={styles.span}>*</span></label>
                <input
                    type="date"
                    name="start_working_date"
                    onChange={handleChange}
                    required
                />
                <br></br> <br></br> 
                {start_working_dateError && <p className={styles.error}>{start_working_dateError}</p>}            
                <div className={styles.button_group}>
                    <button type="button" onClick={handleCansel}
                    >CANCEL</button>
                    <button type="submit"
                        onClick={handleSubmit}
                    >CREATE</button>
                </div>
            </form>
        </div>
    )
}
export default Speaker_popup;