import { useContext } from "react";
import DataContext from "../../../context/dataContext";
import './DeletePopup.css';
import apiRequest from "../../../fanctions/apiRequest";

function AreYouSure(props) {
    const {setIsOpen, setpopupComponent } = useContext(DataContext);
    const handleSubmit = () => {
        setIsOpen(false);
        setpopupComponent(null);
    };
    const handleCansel = () => {
        setIsOpen(false);
        setpopupComponent(null);
    };
    return (
        <div className='form' >
            <h2 style={{ marginTop: '-1rem' }}>Are you sure?</h2>
            <h3 style={{ marginTop: '-1rem' }}>Do you wan't delete {props.data && props.data.name}?</h3>
            {/* <h3 style={{ marginTop: '-1rem' }}>Do you wan't delete {props.data.name}?</h3> */}
        <div className="button-group" style={{ marginTop: '0.1rem' }}>
            <button type="submit" style={{ marginRight: '0.8rem' }} onClick={handleCansel}
            >CANSEL</button>
            <button type="button" style={{ marginLeft: '1.2rem' }}
                onClick={handleSubmit}
            >DELETE</button>
        </div>
        </div>
    )
}
export default AreYouSure;
