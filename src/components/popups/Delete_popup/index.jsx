import { useContext } from "react";
import DataContext from "../../../context/dataContext";
import styles from './style.module.css'
import apiRequest from "../../../fanctions/apiRequest";

function Delete_popup(props) {
    const {setIsOpen, setpopupComponent } = useContext(DataContext);
    const handleCancel = () => {
        setIsOpen(false);
        setpopupComponent(null);
    };
    const handleSubmit = () => {
       
        apiRequest("/users/moveToArchieved", "PUT",{ id:props.data.id , is_active:false})
				.then(
					(response) => {
						if(response.data == null)
						 console.log("response = ", response); 
						},
					(error) => { 
						console.log("error = ", error); 
					}
				);
        setIsOpen(false);
        setpopupComponent(null);
    };

    return (
        <div className={styles.form} >
            <h2 className={styles.h2}>Are you sure?</h2>
            <h3 className={styles.h2}>Do you wan't delete {props.data && props.data.first_name} {props.data && props.data.last_name}?</h3>
        <div className={styles.button_group}>
            <button type="submit" className={styles.button}  onClick={handleCancel}
            >CANCEL</button>
            <button type="button" className={styles.button} onClick={handleSubmit}
            >DELETE</button>
        </div>
        </div>
    )
}
export default Delete_popup;
