import { useState } from 'react'
import styles from './style.module.css'
import './Css.css'





export default function ComponentForSpeakerPopup(props) {
    const [select, setSelect] = useState(true);
    const [numberValue, setNumberValue] = useState(null);
    const [textInput, setTextInput] = useState(null);

    function createCheckFill() {
        console.log(textInput);
        if (numberValue == null || numberValue < 0) {
            alert("Please check the ID")
        }
        else if (textInput == null || !(/^[a-zA-Z ]{3,30}$/.test(textInput))) {
            alert("Please check the name")
        }
        else {
            //here need write the create code
            props.onClick()
        }

    }
    return (
        <div className={styles.ComponentForSpeakerPopup}>
            <h4 className={styles.header}>ADD NEW SPEAKER</h4>
            <h6 className={styles.SpeakerIDText}>Speaker ID<span className={styles.redAsterisk}> *</span></h6>
            <input className='inputSpeakerPopup' type="number" value={numberValue} onChange={e => setNumberValue(e.target.value)}></input >
            <h6 className={styles.nameText} >Name<span className={styles.redAsterisk}> *</span></h6>
            <input className='inputSpeakerPopup' type='text' value={textInput} onChange={e => setTextInput(e.target.value)}></input>
            <label className={styles.switch}>
                <input className='inputSpeakerPopup' type='checkbox' value="available periods" checked={select} id="checkbox" onClick={() => { setSelect(!select) }} />
                <span className={"slider round"}></span>
            </label>
            <span className={styles.ActiveText} style={{ color: select ? 'black' : 'gray' }}>Active</span>
            <button className={styles.ButtonAvailable} onClick={() => { setSelect(!select) }}>AVAILABLE PERIODS</button>
            <button className={styles.cancelButton}>CANCEL</button>
            <button type='submit' className={styles.createButton} onClick={createCheckFill}>CREATE</button>
        </div>
    )
}