import React, { useEffect, useState } from 'react'
import PhoneInput from 'react-phone-input-2'
import 'react-phone-input-2/lib/style.css'
import styles from './style.module.css'
import { useContext } from 'react'
import DataContext from '../../../context/dataContext'
import apiRequest from '../../../fanctions/apiRequest'


export default function User_popup(props) {
	const { setIsOpen, setpopupComponent } = useContext(DataContext)

	let arrayFields = [
		'first_name',
		'last_name',
		'email_adress',
		'phone_num',
		'city',
		'street',
		'house',
		'apartment',
		'id_num',
	]


	function regularField(name, type, value, required) {
		return (
			<>
				<label className={styles.label}>{changeNameField(name)}<span className={styles.red_sterisk}>*</span></label>
				<input
					className={styles.input}
					value={value}
					type={type}
					name={name}
					onChange={handleChange}
					required={required}
				/>
				{showError[name] && <p className={styles.error}>{textError[name]}</p>}
			</>
		)
	}


	function regularPhone_num(value) {
		return (
			<>
				<label className={styles.label}>Phone num<span className={styles.red_sterisk}>*</span></label>
				<PhoneInput
					name="phone_num"
					country={'il'}
					value={value}
					onChange={(e) => {
						handlephone_numChange(e)
					}}
					inputStyle={{
						width: '369px',
						height: '44px',
						paddingLeft: '3rem',
						fontSize: '1rem',
						border: '1px solid #ccc',
						borderRadius: '0.25rem',
						marginBottom: '1rem',
					}}
				/>
				<br />
				{showError.phone_num && <p className={styles.error}>{textError.phone_num}</p>}
			</>
		)
	}


	const [form, setForm] = useState({
		first_name: props.data?.first_name ?? '',
		last_name: props.data?.last_name ?? '',
		email_adress: props.data?.email_adress ?? '',
		phone_num: props.data?.phone_num ?? '',
		city: props.data?.city ?? '',
		street: props.data?.street ?? '',
		house: props.data?.house ?? '',
		apartment: props.data?.apartment ?? '',
		permission: props.data?.permission ?? 1,
		id_num: props.data?.id_num ?? '',
		is_active: props.data?.is_active ?? true,
	})


	const [showError, setShowError] = useState({
		first_name: false,
		last_name: false,
		email_adress: false,
		phone_num: false,
		city: false,
		street: false,
		house: false,
		apartment: false,
		permission: false,
		id_num: false,
	})


	const [textError, setTextError] = useState({
		first_name: 'Please enter your first_name.',
		last_name: 'Please enter your last_name.',
		email_adress: 'Please enter your email_adress.',
		phone_num: 'Please enter your phone_num number.',
		city: 'Please enter your City.',
		street: 'Please enter your Street.',
		house: 'Please enter your House.',
		apartment: 'Please enter your apartment.',
		permission: 'Please enter your permission.',
		id_num: 'Please enter your id_num.',
	})


	const handleChange = (event) => {
		const { name, value } = event.target
		setForm({ ...form, [name]: value })
	}


	const handlephone_numChange = (phone_num) => {
		setForm({ ...form, phone_num: phone_num })
	}


	const handleSubmit = (event) => {
		event.preventDefault()
		let copyTextError = { ...showError }
		for (const key in form) {
			if (key == 'phone_num') {
				let value = form[key].slice(3)

				copyTextError[key] = !value || !/^\d{10}$/.test(value)
				if (copyTextError[key]) {
					let newError = !value
						? 'Please enter your phone_num number.'
						: 'Please enter a valid 10-digit phone number.'
					setTextError((e) => {
						return { ...e, [key]: newError }
					})
				}
			} else if (key == 'email_adress') {
				let value = form[key]
				copyTextError[key] = !value || !/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/.test(value)
				if (copyTextError[key]) {
					let newError = !value
						? 'Please enter your email_adress .'
						: 'Please write a standard email format.'
					setTextError((e) => {
						return { ...e, [key]: newError }
					})
				}
			} else {
				// cheak if the current key exist in copyTextError and ckeck if there is value 
				if (Object.keys(copyTextError).includes(key)) {
					copyTextError[key] = !form[key]
				}
			}
		}


		let checkIfSendDate = true;
		for (const key in copyTextError) {
			if (copyTextError[key]) {
				checkIfSendDate = false;
				break;
			}
		}
		setShowError(copyTextError)


		if (checkIfSendDate) {
			let dataToServer = {...form}
			if(props.data && props.data.id){
				dataToServer.id = props.data.id
			}
			console.log("dataToServer = ",dataToServer);
			apiRequest("/users/create", "POST", dataToServer)
				.then(
					(response) => {
						if (response.data == null)
							alert("error: add new user fails")
						console.log("response = ", response);
						setIsOpen(false)
					},
					(error) => {
						alert("error: add new user fails")
						console.log("error = ", error);
					}
				);
		}
	}

	const handleCancel = () => {
		setIsOpen(false)(null)
	}


	const changeNameField = (text) => {
		let newVale = text.replaceAll('_', ' ')
		newVale = newVale.toLowerCase()
		newVale = newVale.replace(text[0], newVale[0].toUpperCase())
		return newVale
	}


	return (
		<div className={styles.contaunerPopup}>
			<div className={styles.contaunerForm}>
				<h2>ADD/CREATE NEW USER</h2>
				<form className={styles.form}>
					{arrayFields.map((field) => {
						return field == 'phone_num'
							? regularPhone_num(form[field])
							: regularField(field, 'text', form[field], true)
					})}
					<label className={styles.label}>Choose permission<span className={styles.red_sterisk}>*</span></label>
					<select
						className={styles.select}
						defaultValue={''}
						name="permission"
						onChange={handleChange}
						required
					>
						<option value="1">Hospital worker</option>
						<option value="2">Admin</option>
						<option value="3">Parent</option>
					</select>
					{showError.permission && <p className={styles.error}>{textError.permission}</p>}

					<div className={styles.conrainerButton}>
						<button
							className={styles.button1}
							type="button"
							onClick={handleCancel}
						>
							CANCEL
						</button>
						<button className={styles.button2} type="submit" onClick={handleSubmit}>CREATE</button>
					</div>
				</form>
			</div>
		</div>
	)
}
