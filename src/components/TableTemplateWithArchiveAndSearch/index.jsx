import styles from "./style.module.css";

import { useContext, useEffect, useState } from "react";
import PaginationList from "../PaginationList";
import ViewTable from "../ViewTable";
import Search from "../Search/Index";
import { getEntityByNumPage, getNumPagesOfEntity } from "../../fanctions/tableFunctions";
import SwitchActive from "../Buttons/SwitchActive";
import DataContext from "../../context/dataContext";



export default function TableTemplateWithArchiveAndSearch({
	getNumPagesAll,
	getNumPagesArchive,
	getNumPagesSearch,
	getNumPagesArchiveAndSearch,

	allByPageNum,
	archiveByPageNum,
	searchByPageNum,
	archiveAndSearchByPageNum,
	actions,

	pageSize = 10,
	// to include "Loader" when waiting to server
	withLoader = true,

	children,
}) {

	const { setShowLoader } = useContext(DataContext);
	const [SumPages, setSumPages] = useState();
	const [valueOfTable, setValueOfTable] = useState();
	const [currentPage, setCurrentPage] = useState();
	const [isArchives, setIsArchives] = useState(false);
	const [dataOfSearch, setDataOfSearch] = useState("");

	useEffect(() => {
		withLoader && setShowLoader(true)
	}, [])

	// Request for sum pages
	useEffect(() => {
		withLoader && setShowLoader(true)
		if (!isArchives && !dataOfSearch) {
			console.log("!isArchives && !dataOfSearch");
			getNumPagesOfEntity(`${getNumPagesAll}/${pageSize}`, "GET", null, setSumPages, setValueOfTable, (withLoader ? setShowLoader : null))
		} else if (isArchives && !dataOfSearch) {
			console.log("isArchives && !dataOfSearch");
			getNumPagesOfEntity(`${getNumPagesArchive}/${pageSize}`, "GET", null, setSumPages, setValueOfTable, (withLoader ? setShowLoader : null))
		} else if (isArchives && dataOfSearch) {
			console.log("isArchives && dataOfSearch");
			let data = { search: dataOfSearch, size: pageSize }
			console.log("data = ", data);
			getNumPagesOfEntity(`${getNumPagesArchiveAndSearch}`, "POST", data, setSumPages, setValueOfTable, (withLoader ? setShowLoader : null))
		} else if (!isArchives && dataOfSearch) {
			console.log("!isArchives && dataOfSearch");
			let data = { search: dataOfSearch, size: pageSize }
			console.log("data = ", data);
			getNumPagesOfEntity(`${getNumPagesSearch}`, "POST", data, setSumPages, setValueOfTable, (withLoader ? setShowLoader : null))
		} else {
			withLoader && setShowLoader(false)
		}
	}, [isArchives]);


	// Data request for a specific page
	useEffect(() => {
		withLoader && setShowLoader(true)
		if (currentPage) {
			if (!isArchives && !dataOfSearch) {
				console.log("!isArchives && !dataOfSearch");
				getEntityByNumPage(`${allByPageNum}/${currentPage}/${pageSize}`, "GET", null, setValueOfTable, (withLoader ? setShowLoader : null));
			} else if (isArchives && !dataOfSearch) {
				console.log("isArchives && !dataOfSearch");
				getEntityByNumPage(`${archiveByPageNum}/${currentPage}/${pageSize}`, "GET", null, setValueOfTable, (withLoader ? setShowLoader : null));
			} else if (!isArchives && dataOfSearch) {
				console.log("!isArchives && dataOfSearch");
				let data = { "size": pageSize, "currentPage": currentPage, "search": dataOfSearch };
				console.log("data = ", data);
				getEntityByNumPage(`${searchByPageNum}`, "POST", data, setValueOfTable, (withLoader ? setShowLoader : null))
			} else if (isArchives && dataOfSearch) {
				console.log("isArchives && dataOfSearch");
				let data = { "size": pageSize, "currentPage": currentPage, "search": dataOfSearch };
				console.log("data = ", data);
				getEntityByNumPage(`${archiveAndSearchByPageNum}`, "POST", data, setValueOfTable, (withLoader ? setShowLoader : null))
			} else {
				withLoader && setShowLoader(false)
			}
		}
	}, [currentPage]);


	function functionTOSearch(search) {
		if (search && !isArchives) {
			console.log("search && !isArchives");
			let data = { search: search, size: pageSize }
			console.log("data = ", data);
			getNumPagesOfEntity(`${getNumPagesSearch}`, "POST", data, setSumPages, setValueOfTable, (withLoader ? setShowLoader : null))
		} else if (search && isArchives) {
			console.log("search && isArchives");
			let data = { search: search, size: pageSize }
			console.log("data = ", data);
			getNumPagesOfEntity(`${getNumPagesArchiveAndSearch}`, "POST", data, setSumPages, setValueOfTable, (withLoader ? setShowLoader : null))
		} else if (!search) {
			console.log("!search");
			getNumPagesOfEntity(`${getNumPagesAll}/${pageSize}`, "GET", null, setSumPages, setValueOfTable, (withLoader ? setShowLoader : null))
		}
	}
	return (
		<div className={styles.container}>
			<div className={styles.subContainer}>
				<div className={styles.switchAndSearchContainer}>
					<SwitchActive onClick={(e) => { setIsArchives(e) }} />
					<Search funcToDO={functionTOSearch} setDataOfSearch={setDataOfSearch} />
				</div>

				<div>
					<ViewTable data={valueOfTable} pagination={false} actions={actions} />
					<PaginationList pagesCount={SumPages} setCurrentPage={setCurrentPage} limit={pageSize} refresh={isArchives} />
				</div>
				{children && children}
			</div>
		</div>
	);
}


