import { Children, useEffect, useState } from "react";
import PaginationList from "../../components/PaginationList";
import ViewTable from "../ViewTable";
import Search from "../../components/Search/Index";
import { getEntityByNumPage, getNumPagesOfEntity } from "../../fanctions/tableFunctions";
import styles from "./style.module.css";
import PlusButton from "../../components/Buttons/PlusButton";
import SwitchActive from "../../components/Buttons/SwitchActive";



export default function TableTemplateWithSearch({

	getNumPagesAll,
	getNumPagesArchive,
	getNumPagesSearch,
	allByPageNum,
	archiveByPageNum,
	searchByPageNum,
	children,
	pageSize = 10
}) {

	const [SumPages, setSumPages] = useState();
	const [valueOfTable, setValueOfTable] = useState();
	const [currentPage, setCurrentPage] = useState();
	const [isArchives, setIsArchives] = useState(false);
	const [dataOfSearch, setDataOfSearch] = useState("");
	console.log(valueOfTable);



	/*

	getNumPagesAll
	getNumPagesArchive
	getNumPagesSearch

	allByPageNum
	archiveByPageNum
	searchByPageNum

	getNumPagesAll,getNumPagesArchive,getNumPagesSearch,allByPageNum,archiveByPageNum,searchByPageNum

	*/

	// Request for sum pages
	useEffect(() => {
		if (!isArchives) {
			getNumPagesOfEntity(`${getNumPagesAll}/${pageSize}`, "GET", null, setSumPages, setValueOfTable)
		} else {
			getNumPagesOfEntity(`${getNumPagesArchive}/${pageSize}`, "GET", null, setSumPages, setValueOfTable)
		}
	}, [isArchives]);


	// Data request for a specific page
	useEffect(() => {
		if (currentPage) {
			if (!isArchives && !dataOfSearch) {
				getEntityByNumPage(`${allByPageNum}/${currentPage}/${pageSize}`, "GET", null, setValueOfTable);
			} else if (isArchives && !dataOfSearch) {
				getEntityByNumPage(`${archiveByPageNum}/${currentPage}/${pageSize}`, "GET", null, setValueOfTable);
			} else {
				let data = { "size": pageSize, "currentPage": currentPage, "search": dataOfSearch };
				getEntityByNumPage(`${searchByPageNum}`, "POST", data, setValueOfTable)
			}
		}
	}, [currentPage]);


	return (
		<div className={styles.Hospital}>
			<div className={styles.containerUsers}>
				<div className={styles.switchAndSearchContainer}>
					<SwitchActive onClick={(e) => { setIsArchives(e) }} />
					<Search path={`${getNumPagesSearch}`} setValueOfTable={setValueOfTable} size={pageSize} setSumPages={setSumPages} setDataOfSearch={setDataOfSearch} />
				</div>

				<div>
					<ViewTable data={valueOfTable} pagination={false} />
					<PaginationList pagesCount={SumPages} setCurrentPage={setCurrentPage} limit={pageSize} refresh={isArchives} />
				</div>
				{children && children}
			</div>
		</div>
	);
}


