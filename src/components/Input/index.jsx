import { useEffect, useState } from 'react'
import styles from './style.module.css'

// Creator : input
// Support : text , email , password 
// Props : label, placeholder, type, value, setValue,textError, required
// textError is A function that returns a textError


function Input({ ...props }) {
  let { label, placeholder, type, value, setValue,textError, required ,style} = props
  let [valueError, setValueError] = useState(null)

  useEffect(()=>{
   if(textError){
    setValueError(textError)
   }
  },[textError])

  return (
    <div className={styles.input}>
    <label>
      {label && <>label <br /></>}

      <input
        className={ style ? style : valueError ? styles.inputFalse : styles.normal}
        type={type}
        value={value}
        placeholder={placeholder}
        required={required}
        onChange={(e) => setValue(e.target.value)}
      />
    </label>
    
    {valueError && <><br/><span className={styles.errorText}>{valueError}</span></>}
    </div>
  )
}

export default Input
