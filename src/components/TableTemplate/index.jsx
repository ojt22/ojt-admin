import styles from "./style.module.css";

import { useContext, useEffect, useState } from "react";
import PaginationList from "../PaginationList";
import ViewTable from "../ViewTable";
import { getEntityByNumPage, getNumPagesOfEntity } from "../../fanctions/tableFunctions";
import DataContext from "../../context/dataContext";
import { set } from "react-hook-form";



export default function TableTemplate({
	getNumPagesAll,
	allByPageNum,

	pageSize = 10,
	// to include "Loader" when waiting to server
	withLoader = false,
	variableToPath = "",
	children,
}) {

	const { setShowLoader } = useContext(DataContext);

	const [SumPages, setSumPages] = useState();
	const [valueOfTable, setValueOfTable] = useState();
	const [currentPage, setCurrentPage] = useState();
	const [x, setX] = useState([])


	// Request for sum pages
	useEffect(() => {
		withLoader && setShowLoader(true)
		let path = `${getNumPagesAll}/${pageSize}` + (variableToPath ? `/${variableToPath}` : "");
		console.log("path = ", path);
		getNumPagesOfEntity(path, "GET", null, setSumPages, setValueOfTable, withLoader && getNumPagesAll)
	}, []);


	// Request for sum pages
	useEffect(() => {
		if (variableToPath) {
			withLoader && setShowLoader(true)
			let path = `${getNumPagesAll}/${pageSize}` + (variableToPath ? `/${variableToPath}` : "");
			// console.log("path = ", path);
			getNumPagesOfEntity(path, "GET", null, setSumPages, setValueOfTable, withLoader && getNumPagesAll)
		}
	}, [variableToPath]);


	// Data request for a specific page
	useEffect(() => {
		if (currentPage) {
			let path = `${allByPageNum}/${currentPage}/${pageSize}` + (variableToPath ? `/${variableToPath}` : "");
			// console.log("path = ", path);

			getEntityByNumPage(path, "GET", null, setValueOfTable, withLoader && getNumPagesAll);
		}
	}, [currentPage]);
   console.log(valueOfTable);
	
    if (valueOfTable && valueOfTable?.length) {
		return (
			<div className={styles.container}>
				<div className={styles.subContainer}>
					<div>
						<ViewTable data={valueOfTable} pagination={true} />
						<PaginationList pagesCount={SumPages} setCurrentPage={setCurrentPage} limit={pageSize} refresh={false} />
					</div>
					{children && children}
				</div>
			</div>
		)
	}
}


