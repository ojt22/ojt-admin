import { useEffect, useState } from "react";
import styles from "./style.module.css";


export default function Switch({onClick, value = false}) {

    let [direction, setDirection] = useState(false);


    useEffect(()=>{
        if(value != direction){
            setDirection(value)
        }
    },[value])
    
	const handleChange = (data) => {
        console.log("direction = ", direction);
		setDirection(data)
		if (onClick) {
			onClick(data)
		}
	};

    

    return (
        <div className={styles.container} onClick={() => {
                console.log("direction = ", direction);
                handleChange(!direction) 
             }}>
            <div className={styles.containerBull}>
                <div className={!direction ? styles.bullLeft : styles.bullRight}></div>
            </div>
        </div>
    )
}