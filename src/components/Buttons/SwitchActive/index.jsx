import { useEffect, useState } from "react";
import Switch from "../Switch";
import styles from "./style.module.css";

function SwitchActive({ onClick, defaultValue = false }) {

	const [checked, setChecked] = useState(defaultValue);
	const [oneTime, setOneTime] = useState(true);

	const handleChange = (data) => {
		setChecked(data)
		if (onClick) {
			onClick(data)
		}
	};

	useEffect(()=>{
		if(oneTime){
			handleChange(defaultValue)
			setOneTime(false)
		}
	},[oneTime])


	return (
		<>
		{!oneTime &&
		<span className={styles.container}>
			<span className={checked ? styles.on : styles.off} onClick={() => {checked && handleChange(false) }}>All</span>
			<span className={styles.containerSwitch}>
				<Switch
					value={checked}
					onClick={(e) => {
						handleChange(e)
					}}
				/>
			</span>
			<span className={!checked ? styles.on : styles.off} onClick={() => { !checked && handleChange(true) }}>Archieved</span>
		</span>
		}
	</>
	)
}

export default SwitchActive;