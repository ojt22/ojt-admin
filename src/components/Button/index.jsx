import style from "./style.module.css";
// Creator : button
// Props :  buttonText , secondary type Boolean ,
//        ...otherprops = Write exactly like what you write in the button (onClikc)
export default function Button(props) {
  const { buttonText, secondary, ...otherprops } = props;

  return (
    <button
      className={`${style.PrimaryButton} 
      ${secondary ? style.SecondaryButton : ""}`}
      {...otherprops}>
      {buttonText}
    </button>
  );
}
