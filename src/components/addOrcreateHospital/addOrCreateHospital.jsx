import React, { useState } from "react";
import PhoneInput from 'react-phone-input-2';
import 'react-phone-input-2/lib/style.css';
import './addOrCreateHospital.css';
import apiRequest from "../../fanctions/apiRequest";
import { useContext } from "react";
import DataContext from "../../context/dataContext";



// Creator : moshe cohen
function AddOrCreateHospital(props) {
    const { setIsOpen, setpopupComponent } = useContext(DataContext);
    // useEffect(() => {
    //     async function fetchNames() {
    //       try {
    //         const response = await fetch('https://your-server-url/names');
    //         const names = await response.json();
    //         setNames(names);
    //       } catch (error) {
    //         console.error('Error fetching names:', error);
    //       }
    //     }
    
    //     fetchNames();
    //   }, []);
    const [form, setForm] = useState(
        { 
            name: props.data?.name ?? "",
            phone: props.data?.phone ?? "",
            address: props.data?.address ?? "",
            countryCode: props.data?.countryCode ?? "",
            ManagerName: props.data?.ManagerName ?? "Choose manager"
        }
    );
    const [nameError, setNameError] = useState("");
    const [addressError, setAddressError] = useState("");
    const [phoneError, setPhoneError] = useState("");
    const [managerNameError, setManagerName] = useState("");


    const handleChange = (event) => {
        const { name, value } = event.target;
        setForm({ ...form, [name]: value });
        // if (value === "+") {
        //     console.log("uuuuuuuu");
        //     setpopupComponent(<div style={{ width: "100px", height: "100px" }}></div>);
        // }
    };

    const handlePhoneChange = (phone) => {
        setForm({ ...form, phone });
        console.log(`+${form.countryCode} ${form.phone}`);
    };

    const handleSubmit = (event) => {
        let formIsValid = true;
        event.preventDefault();

        if (!form.name) {
            setNameError("Please enter your name");
            formIsValid = false;
            // } else if (!/^[A-Za-z ]+$/.test(form.name)) {
            //     setNameError("Please enter a valid name.");
            //     formIsValid = false;
        } else {
            setNameError("");
        }

        if (!form.address) {
            setAddressError("Please enter your address.");
            formIsValid = false;
        } else {
            setAddressError("");
        }

        if (!form.phone) {
            setPhoneError("Please enter your phone number.");
            formIsValid = false;
        } else if (!/^\d{10}$/.test(form.phone)) {
            setPhoneError("Please enter a valid 10-digit phone number.");
            formIsValid = false;
        } else {
            setPhoneError("");
        }

        if (!form.ManagerName) {
            setManagerName("Please choose a number.");
            formIsValid = false;
        } else if (form.ManagerName < 1 || form.ManagerName > 5) {
            setManagerName("Please choose a number between 1 and 5.");
            formIsValid = false;
        } else {
            setManagerName("");
        }

        if (formIsValid) {
            // perform form submission here
            console.log("Form submitted successfully!");
        }


        // apiRequest("http://example.com","post",form )


    };
    const ToAddNewManager = () => {
        console.log("uuuuuuuu");
        // setIsOpen(false);
        // setpopupComponent(<div style={{width:"100px",height:"100px"}}></div>);
    };

    const handleCansel = () => {
        setIsOpen(false);
        setpopupComponent(null);
    };



    return (
        <div className='form-hospital'>
            <h2>ADD/CREATE NEW HOSPITAL</h2>
            <form>
                <label>Name<span style={{ color: 'red' }}>*</span></label>
                <input
                    placeholder="Enter a name"
                    value={form.name}
                    type="text"
                    name="name"
                    onChange={handleChange}
                    required
                />
                {nameError && <p className="error">{nameError}</p>}
                <label>Phone<span style={{ color: 'red' }}>*</span></label>
                <PhoneInput
                    name='phone'
                    country={'il'}
                    value={form.phone}
                    onChange={handlePhoneChange}
                    inputStyle={{
                        width: "369px",
                        height: "44px",
                        paddingLeft: "3rem",
                        fontSize: "1rem",
                        border: "1px solid #ccc",
                        borderRadius: "0.25rem",
                        marginBottom: '1rem'
                    }}
                />

                <label style={{ marginTop: '1rem' }}>Address<span style={{ color: 'red' }}>*</span></label>
                <input type="text" placeholder="Enter an address" name="address" onChange={handleChange} value={form.address} />
                {addressError && <p className="error">{addressError}</p>}
                <label>Choose manager<span style={{ color: 'red' }}>*</span></label>
                <select
                    // defaultValue={""}
                    value={form.ManagerName}
                    name="ManagerName"
                    onChange={handleChange}
                    required
                    style={{
                        width: "370px",
                    }}>
                    {/* <option value="" disabled selected></option> */}
                    <option value="+" onChange={ToAddNewManager}>To add a new manager</option>
                    {/* {names.map((name, index) => (
                        <option key={index} value={name}>{name}</option>
                    ))} */}
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                </select>
                {managerNameError && <p className="error">{managerNameError}</p>}
                <div className="button-group" style={{ marginTop: '0.5rem' }}>
                    <button type="button" style={{ marginRight: '0.8rem' }} onClick={handleCansel}
                    >CANSEL</button>
                    <button type="submit" style={{ marginLeft: '1.2rem' }}
                        onClick={handleSubmit}
                    >CREATE</button>
                </div>
            </form>
        </div>
    )
}
export default AddOrCreateHospital;