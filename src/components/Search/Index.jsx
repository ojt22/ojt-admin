import { useEffect,useState } from "react";
import apiRequest from "../../fanctions/apiRequest";
import Input from "../Input"
import styles from './style.module.css'
import { IoSearchSharp } from "react-icons/io5";


function Search({ funcToDO, setDataOfSearch }) {

    let [search, setSearch] = useState("");

    function setValue(event) {
        setSearch(event.target.value)
    }
    
    function toSearch() {
        setDataOfSearch(search)
        funcToDO && funcToDO(search)
    }


    return (
        <span className={styles.containerSearch}>
            <div className={styles.containerInput}>
                <input className={styles.styleInput} placeholder={"Search"} value={search} onChange={setValue} />
            </div>
            <div className={styles.wrapIcon} onClick={() => { toSearch() }}><IoSearchSharp /></div>
            {/* <button className={styles.buttonSearch} onClick={() => { toSearch() }}>Search</button> */}
        </span>
    )
}

export default Search;