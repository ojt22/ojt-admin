// import React, { useState } from "react";
import { useState } from "react";
import { useForm } from "react-hook-form";
import Comp from "../createMetricsComp";
// import Input from "../Input";
import styles from "./style.module.css";
import makeApiCall from "../../fanctions/apiRequest";
import Input from "../Input";
import apiRequest from "../../fanctions/apiRequest";
import DataContext from '../../context/dataContext';
import NotePopupMetrics from "../../components/createMetrics/index";
import { useContext } from 'react'


function NotePopup() {
  const { setIsOpen, setpopupComponent } = useContext(DataContext);
    let [flagCancel, setFlagCancel] = useState(true);
    let [flagSave, setFlagSave] = useState(true);
    let [name,setName]=useState("")
    let [GestationBirthWeek,setGestationBirthWeek]=useState("")
    let [userId ,setuserId]=useState()
    let [speakerId ,setSpeakerId]=useState()
 
    function changeColor(e) {
      let button_type = e.target.innerHTML;
      if (button_type == "CANSEL" && flagCancel) {
        setColor1(color2);
        setColor2(color1);
       
      } else if (button_type == "CREATE" && flagSave) {
        setColor1(color2);
        setColor2(color1);
        
      }
    }

    function send(){
      let obj={
      name:name, 
      speaker_id:3,
      parent_id:3,
      gestational_age_date:36,
      birth_date:"2023-12-01",
      sex:"sun",
      id_num:"566",
      id:457
    }
    console.log(obj);
    makeApiCall("api/baby_account/create","POST",obj).then((data)=>{
    
    console.log("00000000000000000000000000");
    setpopupComponent(<NotePopupMetrics/>)
		setIsOpen(true)
      },
      (err)=>{
        console.log(err);
      }
    )
  }
  
      const [color1, setColor1] = useState({
        background: "#FFFFFF 0% 0% no-repeat padding-box",
        color: "blue",
      });
      const [color2, setColor2] = useState({
        background: "#0052A5 0% 0% no-repeat padding-box",
        color: "white",
      });
  return (
    <div className={styles.border}>
      <h1 className={styles.title}>ADD NEW BABY</h1>
     
      <label>
        <span>
          Name <span style={{ color: "red" }}> *</span>
        </span>
        <input
          placeholder="name"
          type="text"
          required
          className={styles.inputStyle}
          onChange={(event)=>{setName(event.target.value)
          console.log("rhrhreaherh");
          }}
        />
        <br />
        <span>
          Gestation birth week <span style={{ color: "red" }}> *</span>
        </span>
        <select className={styles.selectStyle} onChange={(event)=>{setGestationBirthWeek(event.target.value)}}>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
        </select>
        <span>
          Parent user id <span style={{ color: "red" }}> *</span>
        </span>
        <div className={styles.buttonsAndInputs}>
          <input
            placeholder="Parent user name"
            type="text"
            required
            className={styles.shortInputStyle}
            onChange={(event)=>{setuserId(event.target.value)}}
          />
          <br />
          <button className={styles.buttonStyle}>SYNCH</button>
        </div>
        <span>
          speaker id <span style={{ color: "red" }}> *</span>
        </span>
        <div className={styles.buttonsAndInputs2}>
          <input
            placeholder="Parent user name"
            type="text"
            required
            className={styles.shortInputStyle}
            onChange={(event)=>{setSpeakerId(event.target.value)}}
          />
          <br />
          <button className={styles.buttonStyle} disabled={true}>SYNCHED</button>
        </div>
        <button className={styles.buttonDischrgeStyle}>DISCHARGE</button>
        <br/>
        <br/>
        <div className={styles.buttons}>
          <button
          
              onClick={(e) => {
                changeColor(e);
              }}
              className={styles.button1}
              style={color2}
            >CANSEL</button>
          <button
         
           className={styles.button2}
          style={color1}
          onClick={(e) => {
            changeColor(e);
            send()
          
          }}>CREATE</button>
        </div>
      </label>
    </div>
  );
}
export default NotePopup;