import styles from "./style.module.css";

function Comp(props) {
  
  return (
    <>
            <div className={styles.item} >
            <span>{props.text}<span style={{color:"red"}}> *</span></span>
            <br />
            <input  onChange={(e)=>{
              props.setInput(e.target.value)
            }} className={styles.input} type={props.type} required={props.required} 
            />
          </div>
    </>
  );
}
export default Comp;

// import React from "react";
// function Input({ label, value, onChange }) {

//     return (
//       <div>
//         <label>{label}</label>
//         <input type="text" value={value} onChange={onChange} />
//       </div>
//     );
//   };

// export default Input;
