import { useContext, useEffect, useState } from "react";
import DataContext from "../../context/dataContext";
import styles from "./style.module.css";

export default function Loader() {


    let { showLoader } = useContext(DataContext)
    let [loader, setLoader] = useState(false)

    useEffect(()=>{
        setLoader(showLoader)
    },[showLoader])
    
    return (
        // <div className={styles["lds-ripple"]}>
        //     {/* {returnIcon(8,4)} */}
        //     {/* <div>*</div><div>*</div><div>*</div><div>*</div><div>*</div><div>*</div><div>*</div><div>*</div><div>*</div><div>*</div> */}
        //     <div></div><div></div><div></div><div></div>
        // </div>
        <>
            {loader &&
                <div className={styles.container}>
                    <svg className={styles.svg} viewBox="25 25 50 50">
                        <circle className={styles.circle} r="20" cy="50" cx="50"></circle>
                    </svg>
                </div>
            }
        </>

    )
}