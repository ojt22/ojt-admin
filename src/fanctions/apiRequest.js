import axios from 'axios';
// const axios = require('axios')
 axios.defaults.baseURL = "http://localhost:8080";

const apiRequest = async (path, method, databody) => {

    try {
        // axios.defaults.headers.common.Authorization = `Bearer ${localStorage.token}`;
        let options = {
            url:path,
            method: method,
            data: databody
        }

        let res = await axios(options);
        return res.data
    }
    catch (err) {
        // console.log(err);
        throw err;
    }
}

export default apiRequest;

// The function was tested with a real server and everything is fine
// makeApiCall( "get","/login"); 
