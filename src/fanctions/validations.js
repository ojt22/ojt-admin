const email = (email) => {
  // Regular expression to check for a valid email address
  const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

  // Return true if the email matches the regular expression
  return emailRegex.test(email);
};

const password = (pass) => {
  // Regular expression to check for a valid password address
  const passRegex = /^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,}$/;

  // Return true if the email matches the regular expression
  return passRegex.test(pass);
};

export default {
  email,
  password,
};
