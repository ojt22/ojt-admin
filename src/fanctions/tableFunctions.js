import apiRequest from "./apiRequest";


function getNumPagesOfEntity(path, typeQuery, data, setStatePages, setStateValue, setShowLoader) {
    apiRequest(path, typeQuery, data)
    .then(
        (response) => {
            console.log("response = ",response);
            setStatePages(response.data.SumPages);
            setStateValue(response.data.data);
            
            setShowLoader && setShowLoader(false)
        },
        (error) => {
            console.log("error = ", error);

            setShowLoader && setShowLoader(false)
        }
    );
}


function getEntityByNumPage(path, typeQuery, data, setStateValue, setShowLoader) {
    apiRequest(path, typeQuery, data)
    .then(
        (response) => {
            console.log("response = ",response.data);
            setStateValue(response.data);

            setShowLoader && setShowLoader(false)
        },
        (error) => {
            console.log("error = ", error);

            setShowLoader && setShowLoader(false)
        }
    );
}

export {getNumPagesOfEntity, getEntityByNumPage} 
