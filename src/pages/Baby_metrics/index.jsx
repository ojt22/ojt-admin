import TableTemplateWithSearch from "../../components/TableTemplateWithSearch";
import TableTemplate from "../../components/TableTemplate";

import TableTemplateWithArchiveAndSearch from "../../components/TableTemplateWithArchiveAndSearch";
	


export default function Baby_metrics() {


	return (
		<TableTemplateWithArchiveAndSearch
		getNumPagesAll={"baby_metrics/getSumPage"}
		allByPageNum={"baby_metrics/getAll"}
		pageSize={2}
		>
		
		</TableTemplateWithArchiveAndSearch>
	
	);
}
