import TableTemplate from '../../components/TableTemplate'
import DataContext from '../../context/dataContext'
import styles from './style.module.css'
import PlusButton from '../../components/Buttons/PlusButton'
import Speaker_popup from '../../components/popups/Speaker_popup'
import { useContext } from 'react'
export default function Speaker() {
  const { setIsOpen, setpopupComponent } = useContext(DataContext)

  const handleClosePopup = () => {
    setpopupComponent(<Speaker_popup/>)
    setIsOpen(true)
  }

  return (
    <TableTemplate
      getNumPagesAll={'speaker/getSumPage'}
      allByPageNum={'speaker/getAll'}
      withLoader={false}
      pageSize={10}
    >
      <div className={styles.plusContainer}>
        <PlusButton onClick={handleClosePopup} />
      </div>
    </TableTemplate>
  )
}
