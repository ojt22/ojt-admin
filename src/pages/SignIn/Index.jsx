import { useRef, useState } from "react"
import imeg1 from './imeg1.png'
import Input from "../Index";
import style from "./style.module.css";

const SignIn = () => {



    return (<>
        <div className="container1">
            <h3 className={style.h3}>SignIn</h3>
            <form className={style.form} >

                <div className="header">

                </div>
                <div className="sign" />
                <div className={style.inputs}>
                    <Input type={"email"} placeholder={"email"} label={"email"} textError={"הכנסת יותר מ 5 אותיות"}
                        error={(value) => {
                            if (value.length > 5) {
                                return false
                            }
                            return true
                        }} /> <Input type={"pwd"} placeholder={"pwd"} label={"password"} textError={"הכנסת יותר מ 5 אותיות"}
                            error={(value) => {
                                if (value.length > 5) {
                                    return false
                                }
                                return true
                            }} />

                    <button className={style.buttons}><img className={style.img} src={imeg1} alt="google" /></button>
                    <button className={style.btn}>Sign In</button>
                </div>
            </form>

        </div>

    </>)

}

export default SignIn
