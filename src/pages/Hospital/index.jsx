import styles from "./style.module.css";
import PlusButton from "../../components/Buttons/PlusButton";
import TableTemplateWithArchiveAndSearch from "../../components/TableTemplateWithArchiveAndSearch";
import { useContext } from 'react'
import DataContext from '../../context/dataContext';
// import AddOrCreateHospital from '../../components/addOrcreateHospital/addOrCreateHospital';
import { CiEdit, } from "react-icons/ci";
import { BsTrash3 } from "react-icons/bs";
import IconTable from "../../components/IconTable";
import Popup from "reactjs-popup";


export default function Hospital() {
	const { setIsOpen, setpopupComponent } = useContext(DataContext);

	const handleClosePopup = () => {
		// eslint-disable-next-line react/jsx-no-undef
		setpopupComponent()
		setIsOpen(true)
	}
	// let actions = [
	// 	{ lable: "ACTIONS", Component: (data) => <IconTable data={data} Icon={CiEdit} PopUp={AddOrCreateHospital} /> },
	// 	// {lable: " " ,Component : IconTable , Icon : BsTrash3 , PopUp:   }
	// ]

	return (
		<TableTemplateWithArchiveAndSearch
			getNumPagesAll={"hospital/getPagesNum"}
			getNumPagesArchive={"hospital/getPagesNumOfArchive"}
			getNumPagesSearch={"hospital/getPagesNumOfSearch"}
			getNumPagesArchiveAndSearch={"hospital/getPagesNumOfArchiveAndSearch"}

			allByPageNum={"hospital/getByPageNum"}
			archiveByPageNum={"hospital/getArchiveByPageNum"}
			searchByPageNum={"hospital/getSearchByPageNum"}
			archiveAndSearchByPageNum={"hospital/getArchiveAndSearchByPageNum"}

			// actions={actions}
			withLoader={false}
			// ComponePopUpPen={AddOrCreateHospital}
			// ComponePopUpTrash={} 

			pageSize={2}
		>
			<div className={styles.plusContainer}>
				<PlusButton onClick={handleClosePopup} />
			</div>
		</TableTemplateWithArchiveAndSearch>
	);
}