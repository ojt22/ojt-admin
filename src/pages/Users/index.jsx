import styles from "./style.module.css";
import PlusButton from "../../components/Buttons/PlusButton";
import TableTemplateWithArchiveAndSearch from "../../components/TableTemplateWithArchiveAndSearch";
import { useContext } from 'react'
import DataContext from '../../context/dataContext';
import User_popup from '../../components/popups/‏‏User_popup'
import Delete_popup from '../../components/popups/Delete_popup'
import { CiEdit, } from "react-icons/ci";
import { BsTrash3 } from "react-icons/bs";
import IconTable from "../../components/IconTable";


export default function Users() {
	const {setIsOpen,setpopupComponent} = useContext(DataContext);

	const handleClosePopup = () => {		
		setpopupComponent(<User_popup/>)
		setIsOpen(true)
	  }

	
	let actions = [
	  {lable: "ACTIONS" ,Component : (data) => <IconTable data={data} Icon={CiEdit} PopUp={User_popup}/> },
	  {lable: " " ,Component : (data) => <IconTable data={data} Icon={BsTrash3} PopUp={Delete_popup}/> }
	]
	return (
		<TableTemplateWithArchiveAndSearch
			getNumPagesAll={"users/getPagesNum"}
			getNumPagesArchive={"users/getPagesNumOfArchive"}
			getNumPagesSearch={"users/getPagesNumOfSearch"}
			getNumPagesArchiveAndSearch={"users/getPagesNumOfArchiveAndSearch"}
			
			allByPageNum={"users/getByPageNum"}
			archiveByPageNum={"users/getArchiveByPageNum"}
			searchByPageNum={"users/getSearchByPageNum"}
			archiveAndSearchByPageNum={"users/getArchiveAndSearchByPageNum"}

			actions ={actions}
			pageSize={10}

			withLoader={false}
		>
			<div className={styles.plusContainer}>
			<PlusButton onClick={handleClosePopup}/>
			</div>
		</TableTemplateWithArchiveAndSearch>
	);
}

