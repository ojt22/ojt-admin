import styles from "./style.module.css";
import PlusButton from "../../components/Buttons/PlusButton";
import TableTemplateWithSearch from "../../components/TableTemplateWithSearch";
import { useContext, useEffect } from 'react'
import DataContext from '../../context/dataContext';
import NotePopup from "../../components/editViewBaby/editViewBaby";
import TableTemplateWithArchiveAndSearch from "../../components/TableTemplateWithArchiveAndSearch";
import IconTable from "../../components/IconTable";
import { CiEdit } from "react-icons/ci";
import ContextManagment from "../../context/dataContext";
import TableTemplate from "../../components/TableTemplate";
import { log } from "debug/src/browser";

export default function Baby_account() {

	const { setIsOpen, setpopupComponent } = useContext(DataContext);
	const { dataBaby } = useContext(DataContext);
	const handleClosePopup = () => {
		// eslint-disable-next-line react/jsx-no-undef

		setpopupComponent(<NotePopup />)
		setIsOpen(true)
	}


	let actions = [
		{ lable: "ACTIONS", Component: (data) => <IconTable data={data} Icon={CiEdit} /> },

	]

	return (
		<>
			<TableTemplateWithArchiveAndSearch
				getNumPagesAll={"api/baby_account/getPagesNum"}
				getNumPagesArchive={"api/baby_account/getPagesNumOfbaby_accountsArchive"}
				getNumPagesSearch={"api/baby_account/getPagesNumOfSearchBaby_account"}
				allByPageNum={"api/baby_account/getBaby_accountByPageNum"}
				archiveByPageNum={"api/baby_account/getArchivebaby_accountByPageNum"}
				searchByPageNum={"api/baby_account/getSearchBaby_accountByPageNum"}
				pageSize={10}
				actions={actions}
			>
				<div className={styles.plusContainer}>
					<PlusButton onClick={handleClosePopup} />
				</div>
			</TableTemplateWithArchiveAndSearch>
			{
				dataBaby &&
				<TableTemplate
					pageSize={4}
					variableToPath={dataBaby.id}
					getNumPagesAll={"record/getPagesNum"}
					allByPageNum={"record/getByPageNumOfBabyId"}
				/>
			}
		</>
	);
}
