// import Hospital from "../Hospital";
import Speaker from "../Speaker";
import Users from "../Users";
import styles from "./style.module.css"


export default function Home() {
    return (
        <div>
            <Users />
            <br />
            <br />
            <Speaker />
        </div>
    )
}