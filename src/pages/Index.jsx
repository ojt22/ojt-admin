import { useEffect, useState } from 'react'
import styles from './style.module.css'

// Creator : input
// Support : text , tel , email , password , number
// Props : label, placeholder, type , error ()
// error is A function that returns a boolean


function Input({ ...props }) {
  const { label, placeholder, type, error, textError } = props
  const [text, setText] = useState("")
  const [bool, setBool] = useState(true)

  useEffect(() => {
    setBool(error(text));
  }, [text])
  console.log(error);
  return (
    <div className={styles.input}>
      <label>
        {label}
        <br />
        <input
          className={bool === true ? styles.inputTrue : styles.inputFalse}
          type={type}
          value={text}
          placeholder={placeholder}
          required={true}
          onChange={(e) => setText(e.target.value)}
        />
      </label>

      {!bool && <><br /><span className={styles.errorText}>{textError}</span></>}
    </div>
  )
}

export default Input
