import React from 'react'
import styles from "./style.module.css"

// Creator : ___ ____
// support : text, tel, email
// props : 
//       type: 

// addition props : 
//       min : 
//       max : 
//       pattern


export default function Name({ style = {}, className, ...props }) {

   return (
      <div className={`${styles.Name} ${className}`} style={style} {...props} >

      </div>
   )
}