
import React, { useEffect, useState } from 'react'
import DataContext from './dataContext';
import { fakeUsers } from '../data/fakeData';

// Creator : moshe cohen
// main context for all data that we want to provide
function ContextManagment({ children }) {
	const [users, setUsers] = useState(fakeUsers);
	const [popupComponent, setpopupComponent] = useState(null);
	const [isOpen, setIsOpen] = useState(false);
	// to show loading
	const [showLoader, setShowLoader] = useState(false);
	// state of alert message
	const [alertMessage, setAlertMessage] = useState({ success: "", warning: "", error: "" });
	//Data about a baby after press on spesific row in baby table
	const [dataBaby,setDataBaby] = useState("");
	console.log(dataBaby);
	// function to create alert message (type 1 - to success, type 2 - to warning, type 3 - to error)
	function createAlertMessage(text, type) {
		setAlertMessage((e) => {
			let data = { ...e }
			if (type == 1 && !data.success) {
				data.success = text
				return data
			} else if (type == 2 && !data.warning) {
				data.warning = text
				return data
			} else if (type == 3 && !data.error) {
				data.error = text
				return data
			}
		})
	}

     console.log(dataBaby);

	return <DataContext.Provider value={{
		users,
		setUsers,
		popupComponent,
		setpopupComponent,
		isOpen,
		setIsOpen,
		setAlertMessage,
		createAlertMessage,
		alertMessage,
		showLoader, 
		setShowLoader,
		dataBaby,
		setDataBaby
	}}>
		{children}
	</DataContext.Provider>
}
export default ContextManagment;

