export const fakeUsers = [
    {
        user_ID:3223,
        email_adress:"moshe@gmail.com",
        permission: "admin",
        active : true, 
        firstName:"moshe", 
        lastName:"cohen", 
        created_date: new Date()
    },{
        user_ID:4334,
        email_adress:"yossi@gmail.com",
        permission: "user",
        active : true, 
        firstName:"yossi", 
        lastName:"levi", 
        created_date: new Date()
    }
]

