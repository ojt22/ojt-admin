import { Route, Routes } from "react-router-dom";
import Home from "../pages/Home";
import SignIn from "../pages/SignIn";
import SignUp from "../pages/SignUp";
import Users from "../pages/Users";
import Speaker from "../pages/Speaker";
import Hospital from "../pages/Hospital";

export default function MainRouter() {

    
    return (
        <Routes>
            <Route path="/" element={<Home />}></Route>
            <Route path="/hospital" element={<Hospital />}></Route>
            <Route path="/users" element={<Users />}></Route>
            <Route path="/speakers" element={<Speaker/>}></Route>
            <Route path="/babies" element={<h1>babies</h1>}></Route>
            <Route path="*" element={<SignIn />} />
        </Routes>
    )
}